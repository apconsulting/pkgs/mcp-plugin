# MCP Plugin

Per installare il paccheto sull'applicativo da controllare:

```go test
go get gitlab.com/apconsulting/pkgs/mcp-plugin@latest
```

### **ATTENZIONE**:

> Ricordarsi di includere `gitlab.com/apconsulting` nel `GOPRIVATE` se si vuole scaricare direttamente dal nostro repository, evitando pkg.go.dev e la sua "cache"

<br />
Aggiungere comunicazione per Iris:

```go
import (
	...

	irisHost "github.com/kataras/iris/v12/core/host"
	mcpModels "gitlab.com/apconsulting/pkgs/mcp-models"
)

	...

	mcpPlugin := plugin.New(nil, func(s string) {
		fmt.Println("codice per pacchetti in ricezione", s)
	})


	mcpConfigurator := func(su *iris.Supervisor) {
		su.RegisterOnServe(func(th irisHost.TaskHost) {
			// comunicazioner verso il SARK
			info := mcpModels.InfoPacket{
				...
			}

			mcpPlugin.SendToMCP(info)
		})
	}

	runner := iris.Addr(":8080", mcpConfigurator)
	app.Run(runner)

```
