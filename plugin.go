package plugin

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	models "gitlab.com/apconsulting/pkgs/mcp-models"
)

type MCPPlugin struct {
	callback func(string)
	inStream *os.File
}

// Crea un nuovo plugin MCP e lo inizializza, settando lo stream di ingresso specificato (se = nil,
// utilizza stdin) e la funzione di callback da invocare alla ricezione di un payload valido su questo stream.
func New(in *os.File, cb func(string)) *MCPPlugin {
	plug := MCPPlugin{}

	plug.Init(in, cb)

	return &plug
}

// Inizializza il plugin MCP, settando lo stream di ingresso specificato (se = nil, utilizza stdin) e
// la funzione di callback da invocare alla ricezione di un payload valido su questo stream.
func (p *MCPPlugin) Init(in *os.File, cb func(string)) {
	if in == nil {
		in = os.Stdin
	}

	p.inStream = in
	p.callback = cb

	if p.callback != nil {
		// Resta in ascolto sullo stream fino a che questo non viene chiuso
		go p.listen(in)
	}
}

func (p *MCPPlugin) listen(in *os.File) {
	buf := bufio.NewScanner(in)
	for buf.Scan() {
		t := buf.Text()

		if p.callback != nil && strings.HasPrefix(t, models.MCP_PREFIX) {
			p.callback(strings.Trim(strings.TrimPrefix(t, models.MCP_PREFIX), " "))
		}
	}
}

// Effettua il json.Marshal del ClientInfo, aggiunge il prefisso MCP e invia a stdout
func (p *MCPPlugin) SendToMCP(pl models.InfoPacket) error {
	// var info models.InfoPacket

	// info.Description = pl.Description
	// info.VersionTag = pl.VersionTag
	// info.Vars = pl.Vars

	js, err := json.Marshal(pl)
	if err != nil {
		return err
	}

	_, err = fmt.Println(models.MCP_PREFIX, string(js))
	return err
}
